<?php return array(
    'root' => array(
        'name' => 'funadmin/webmanadmin',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '3f25977036835ddff28087b0969ee72c7db48e4a',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'brick/math' => array(
            'pretty_version' => '0.9.3',
            'version' => '0.9.3.0',
            'reference' => 'ca57d18f028f84f777b2168cd1911b0dee2343ae',
            'type' => 'library',
            'install_path' => __DIR__ . '/../brick/math',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/annotations' => array(
            'pretty_version' => '1.14.3',
            'version' => '1.14.3.0',
            'reference' => 'fb0d71a7393298a7b232cbf4c8b1f73f3ec3d5af',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/annotations',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/deprecations' => array(
            'pretty_version' => 'v1.0.0',
            'version' => '1.0.0.0',
            'reference' => '0e2a4f1f8cdfc7a92ec3b01c9334898c806b30de',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/deprecations',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/inflector' => array(
            'pretty_version' => '2.0.4',
            'version' => '2.0.4.0',
            'reference' => '8b7ff3e4b7de6b2c84da85637b59fd2880ecaa89',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/inflector',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/lexer' => array(
            'pretty_version' => '2.1.0',
            'version' => '2.1.0.0',
            'reference' => '39ab8fcf5a51ce4b85ca97c7a7d033eb12831124',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/lexer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'ezyang/htmlpurifier' => array(
            'pretty_version' => 'v4.16.0',
            'version' => '4.16.0.0',
            'reference' => '523407fb06eb9e5f3d59889b3978d5bfe94299c8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ezyang/htmlpurifier',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'firebase/php-jwt' => array(
            'pretty_version' => 'v6.4.0',
            'version' => '6.4.0.0',
            'reference' => '4dd1e007f22a927ac77da5a3fbb067b42d3bc224',
            'type' => 'library',
            'install_path' => __DIR__ . '/../firebase/php-jwt',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'funadmin/webmanadmin' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '3f25977036835ddff28087b0969ee72c7db48e4a',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'graham-campbell/result-type' => array(
            'pretty_version' => 'v1.1.0',
            'version' => '1.1.0.0',
            'reference' => 'a878d45c1914464426dc94da61c9e1d36ae262a8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../graham-campbell/result-type',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'gregwar/captcha' => array(
            'pretty_version' => 'v1.1.9',
            'version' => '1.1.9.0',
            'reference' => '4bb668e6b40e3205a020ca5ee4ca8cff8b8780c5',
            'type' => 'captcha',
            'install_path' => __DIR__ . '/../gregwar/captcha',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '7.5.0',
            'version' => '7.5.0.0',
            'reference' => 'b50a2a1251152e43f6a37f0fa053e730a67d25ba',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.2',
            'version' => '1.5.2.0',
            'reference' => 'b94b2807d85443f9719887892882d0329d1e2598',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '2.4.3',
            'version' => '2.4.3.0',
            'reference' => '67c26b443f348a51926030c83481b85718457d3d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/collections' => array(
            'pretty_version' => 'v8.83.27',
            'version' => '8.83.27.0',
            'reference' => '705a4e1ef93cd492c45b9b3e7911cccc990a07f4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/collections',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/contracts' => array(
            'pretty_version' => 'v8.83.27',
            'version' => '8.83.27.0',
            'reference' => '5e0fd287a1b22a6b346a9f7cd484d8cf0234585d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/macroable' => array(
            'pretty_version' => 'v8.83.27',
            'version' => '8.83.27.0',
            'reference' => 'aed81891a6e046fdee72edd497f822190f61c162',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/macroable',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/redis' => array(
            'pretty_version' => 'v8.83.27',
            'version' => '8.83.27.0',
            'reference' => '0fee121324054226823a59623fab3d98ad88fbd5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/redis',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/support' => array(
            'pretty_version' => 'v8.83.27',
            'version' => '8.83.27.0',
            'reference' => '1c79242468d3bbd9a0f7477df34f9647dde2a09b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/support',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'intervention/image' => array(
            'pretty_version' => '2.7.2',
            'version' => '2.7.2.0',
            'reference' => '04be355f8d6734c826045d02a1079ad658322dad',
            'type' => 'library',
            'install_path' => __DIR__ . '/../intervention/image',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'maennchen/zipstream-php' => array(
            'pretty_version' => '2.2.6',
            'version' => '2.2.6.0',
            'reference' => '30ad6f93cf3efe4192bc7a4c9cad11ff8f4f237f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../maennchen/zipstream-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'markbaker/complex' => array(
            'pretty_version' => '3.0.2',
            'version' => '3.0.2.0',
            'reference' => '95c56caa1cf5c766ad6d65b6344b807c1e8405b9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../markbaker/complex',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'markbaker/matrix' => array(
            'pretty_version' => '3.0.1',
            'version' => '3.0.1.0',
            'reference' => '728434227fe21be27ff6d86621a1b13107a2562c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../markbaker/matrix',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'monolog/monolog' => array(
            'pretty_version' => '2.9.0',
            'version' => '2.9.0.0',
            'reference' => 'e1c0ae1528ce313a450e5e1ad782765c4a8dd3cb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../monolog/monolog',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'myclabs/php-enum' => array(
            'pretty_version' => '1.8.3',
            'version' => '1.8.3.0',
            'reference' => 'b942d263c641ddb5190929ff840c68f78713e937',
            'type' => 'library',
            'install_path' => __DIR__ . '/../myclabs/php-enum',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nesbot/carbon' => array(
            'pretty_version' => '2.66.0',
            'version' => '2.66.0.0',
            'reference' => '496712849902241f04902033b0441b269effe001',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nesbot/carbon',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nikic/fast-route' => array(
            'pretty_version' => 'v1.3.0',
            'version' => '1.3.0.0',
            'reference' => '181d480e08d9476e61381e04a71b34dc0432e812',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nikic/fast-route',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'opis/closure' => array(
            'pretty_version' => '3.6.3',
            'version' => '3.6.3.0',
            'reference' => '3d81e4309d2a927abbe66df935f4bb60082805ad',
            'type' => 'library',
            'install_path' => __DIR__ . '/../opis/closure',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpoffice/phpspreadsheet' => array(
            'pretty_version' => '1.28.0',
            'version' => '1.28.0.0',
            'reference' => '6e81cf39bbd93ebc3a4e8150444c41e8aa9b769a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoffice/phpspreadsheet',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpoption/phpoption' => array(
            'pretty_version' => '1.9.1',
            'version' => '1.9.1.0',
            'reference' => 'dd3a383e599f49777d8b628dadbb90cae435b87e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoption/phpoption',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'reference' => 'e616d01114759c4c489f93b099585439f795fe35',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.1',
            'version' => '1.1.0.0',
            'reference' => 'cb6ce4845ce34a8ad9e68117c10ee90a29919eba',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0.0 || 2.0.0 || 3.0.0',
                1 => '1.0|2.0',
            ),
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'ramsey/collection' => array(
            'pretty_version' => '1.3.0',
            'version' => '1.3.0.0',
            'reference' => 'ad7475d1c9e70b190ecffc58f2d989416af339b4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ramsey/collection',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'ramsey/uuid' => array(
            'pretty_version' => '4.2.3',
            'version' => '4.2.3.0',
            'reference' => 'fc9bb7fb5388691fd7373cd44dcb4d63bbcf24df',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ramsey/uuid',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'rhumsaa/uuid' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '4.2.3',
            ),
        ),
        'symfony/console' => array(
            'pretty_version' => 'v5.4.22',
            'version' => '5.4.22.0',
            'reference' => '3cd51fd2e6c461ca678f84d419461281bd87a0a8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/console',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => 'e8b495ea28c1d97b5e0c121748d6f9b53d075c66',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/finder' => array(
            'pretty_version' => 'v5.4.21',
            'version' => '5.4.21.0',
            'reference' => '078e9a5e1871fcfe6a5ce421b539344c21afef19',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/finder',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '5bbc823adecdae860bb64756d639ecfec17b050a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-grapheme' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '511a08c03c1960e08a883f4cffcacd219b758354',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-grapheme',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '19bd1e4fcd5b91116f14d8533c57831ed00571b6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '9e8ecb5f92152187c4799efd3c96b78ccab18ff9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '7a6ff3f1959bb01aefccb463a0f2cd3d3d2fd936',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php81' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '707403074c8ea6e2edaf8794b0157a0bfa52157a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php81',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '4b426aac47d6427cc1a1d0f7e2ac724627f5966c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/string' => array(
            'pretty_version' => 'v5.4.22',
            'version' => '5.4.22.0',
            'reference' => '8036a4c76c0dd29e60b6a7cafcacc50cf088ea62',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/string',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation' => array(
            'pretty_version' => 'v5.4.22',
            'version' => '5.4.22.0',
            'reference' => '9a401392f01bc385aa42760eff481d213a0cc2ba',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '136b19dd05cdf0709db6537d058bcab6dd6e2dbe',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.3',
            ),
        ),
        'topthink/think-cache' => array(
            'pretty_version' => 'v2.0.6',
            'version' => '2.0.6.0',
            'reference' => '75a56b24affc65b51688fd89ada48c102757fd74',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-container' => array(
            'pretty_version' => 'v2.0.5',
            'version' => '2.0.5.0',
            'reference' => '2189b39e42af2c14203ed4372b92e38989e9dabb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-helper' => array(
            'pretty_version' => 'v3.1.6',
            'version' => '3.1.6.0',
            'reference' => '769acbe50a4274327162f9c68ec2e89a38eb2aff',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-helper',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-image' => array(
            'pretty_version' => 'v1.0.7',
            'version' => '1.0.7.0',
            'reference' => '8586cf47f117481c6d415b20f7dedf62e79d5512',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-image',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-orm' => array(
            'pretty_version' => 'v2.0.60',
            'version' => '2.0.60.0',
            'reference' => '8bc34a4307fa27186c0e96a9b3de3cb23aa1ed46',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-orm',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-template' => array(
            'pretty_version' => 'v2.0.9',
            'version' => '2.0.9.0',
            'reference' => '6d25642ae0e306166742fd7073dc7a159e18073c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-template',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-validate' => array(
            'pretty_version' => 'v2.0.2',
            'version' => '2.0.2.0',
            'reference' => '857f9bffc1a09a41e3969a19726cb04315848f0f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-validate',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'vlucas/phpdotenv' => array(
            'pretty_version' => 'v5.4.1',
            'version' => '5.4.1.0',
            'reference' => '264dce589e7ce37a7ba99cb901eed8249fbec92f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../vlucas/phpdotenv',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'voku/portable-ascii' => array(
            'pretty_version' => '1.6.1',
            'version' => '1.6.1.0',
            'reference' => '87337c91b9dfacee02452244ee14ab3c43bc485a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../voku/portable-ascii',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/captcha' => array(
            'pretty_version' => 'v1.0.1',
            'version' => '1.0.1.0',
            'reference' => '69d214cacf6c123d205bfe8061c08a54f4a4c7e1',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/captcha',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/console' => array(
            'pretty_version' => 'v1.2.33',
            'version' => '1.2.33.0',
            'reference' => '081cc2ccb41c48fb6a0e30999e1dd7704efe9e93',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/console',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/event' => array(
            'pretty_version' => 'v1.0.3',
            'version' => '1.0.3.0',
            'reference' => 'e36fb7fd158ad7d50a082b93f532225ca13b964f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/event',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/gateway-worker' => array(
            'pretty_version' => 'v1.0.5',
            'version' => '1.0.5.0',
            'reference' => '9954c7c05934a5938eee79419fc27b4221094233',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/gateway-worker',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/redis-queue' => array(
            'pretty_version' => 'v1.2.4',
            'version' => '1.2.4.0',
            'reference' => '81667bf9ab3c1256e2c2f61b9d41d53791b8b34b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/redis-queue',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/think-cache' => array(
            'pretty_version' => 'v1.0.2',
            'version' => '1.0.2.0',
            'reference' => '0420d03a564e3513b7578ec475c6699ec02cd081',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/think-cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webman/think-orm' => array(
            'pretty_version' => 'v1.1.0',
            'version' => '1.1.0.0',
            'reference' => 'e044e4ca66d387f489018a998c9d1e409ffc40b6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webman/think-orm',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/crontab' => array(
            'pretty_version' => 'v1.0.4',
            'version' => '1.0.4.0',
            'reference' => 'b9280b14941840d1b8542633d60ef303cefe3875',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/crontab',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/gateway-worker' => array(
            'pretty_version' => 'v3.0.28',
            'version' => '3.0.28.0',
            'reference' => 'a7dffc53403133131a51b9fd3c6c6d70869cb6d3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/gateway-worker',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/redis' => array(
            'pretty_version' => 'v2.0.1',
            'version' => '2.0.1.0',
            'reference' => '284f93cccc03603e616cf96b8cab847fe6b33b6a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/redis',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/redis-queue' => array(
            'pretty_version' => 'v1.1.0',
            'version' => '1.1.0.0',
            'reference' => 'e325f5b09cd170a327597876f1d659cd81510388',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/redis-queue',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/webman-framework' => array(
            'pretty_version' => 'v1.5.2',
            'version' => '1.5.2.0',
            'reference' => '919305abe90a43c3c42b5fa33654603309b70462',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/webman-framework',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/workerman' => array(
            'pretty_version' => 'v4.1.9',
            'version' => '4.1.9.0',
            'reference' => '1f92d02c26106b5fbe6f61ea776198aad6e426f7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/workerman',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
