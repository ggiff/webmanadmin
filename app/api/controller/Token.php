<?php
namespace app\api\controller;

use fun\auth\Api;
use support\Request;

/**
 * 生成token
 */
class Token extends  Api
{
    protected $noAuth = ['*'];

    public function __construct()
    {
        parent::__construct();
    }

    public function build()
    {

        $class = ucwords('\\fun\\auth\\'.ucfirst($this->type).'Token');
        $token = $class::instance();
        return $token->build();

    }
    public function refresh()
    {
        $class = ucwords('\\fun\\auth\\'.ucfirst($this->type).'Token');
        $token = $class::instance();
        return $token->refresh();

    }

}